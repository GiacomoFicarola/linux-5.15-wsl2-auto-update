[![pipeline status](https://gitlab.com/GiacomoFicarola/linux-5.15-wsl2-auto-update/badges/main/pipeline.svg)](https://gitlab.com/GiacomoFicarola/linux-5.15-wsl2-auto-update/-/commits/main)            [![coverage report](https://gitlab.com/GiacomoFicarola/linux-5.15-wsl2-auto-update/badges/main/coverage.svg)](https://gitlab.com/GiacomoFicarola/linux-5.15-wsl2-auto-update/-/commits/main)            [![Latest Release](https://gitlab.com/GiacomoFicarola/linux-5.15-wsl2-auto-update/-/badges/release.svg)](https://gitlab.com/GiacomoFicarola/linux-5.15-wsl2-auto-update/-/releases)



A CI/CD pipeline to automatically compile the kernel for WSL2 updated to the latest stable version.