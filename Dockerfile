# Usa Debian Slim come base image
FROM debian:stable-slim

# Installa i pacchetti necessari e pulisci la cache
RUN apt-get update && \
        apt-get install -y xmlstarlet bc python3 jq wget curl build-essential flex bison libssl-dev libelf-dev dwarves && \
        apt-get autoclean -y && apt-get autoremove -y && apt-get clean -y
